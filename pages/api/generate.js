import { Configuration, OpenAIApi } from "openai";
import NextCors from "nextjs-cors";

var whitelist = ["http://localhost:8081"];
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error("Not allowed by CORS"));
        }
    },
};

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {
    NextCors(req, res, {
        // Options
        methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
        origin: "*",
        optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    });
    if (!configuration.apiKey) {
        res.status(500).json({
            error: {
                message: "OpenAI API key not configured, please follow instructions in README.md",
            },
        });
        if (!configuration.apiKey) {
            res.status(500).json({
                error: {
                    message: "OpenAI API key not configured, please follow instructions in README.md",
                },
            });
            return;
        }

        const species = req.body.species || "azeaze";
        const class_ = req.body.class_ || "azeaze";
        const job = req.body.job || "azeaze";
        const gender = req.body.gender || "";

        try {
            const completion = await openai.createImage({
                prompt: species + " " + class_ + " " + job + " " + gender,
                n: 2,
            });
            res.status(200).json({ result: completion.data });
        } catch (error) {
            // Consider adjusting the error handling logic for your use case
            if (error.response) {
                console.error(error.response.status, error.response.data);
                res.status(error.response.status).json(error.response.data);
            } else {
                console.error(`Error with OpenAI API request: ${error.message}`);
                res.status(500).json({
                    error: {
                        message: "An error occurred during your request. " + error.message,
                    },
                });
            }
        }
    }
}

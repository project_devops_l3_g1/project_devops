# RPG Character Generator

This project create character cards for DnD games using ChatGPT to make the backstory and DALL-E to generate a picture for the character.

# Start Project

To start the project you need to use this commands :

```
    docker build . -t monchatestorange/openai

    docker run  -p 8081:3000 --env-file .env -v $pwd:/app webpage*
```